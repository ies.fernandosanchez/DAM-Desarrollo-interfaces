<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <head>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"/>
                <link rel="stylesheet" type="text/css" href="main.css" />
                <style>

                </style>
            </head>
            <body>


                <div class="container px-4 py-5" id="featured-3">
                    <h2 class="pb-2 border-bottom">Bibliotecas</h2>
                    <div class="row g-4 py-5 row-cols-1 row-cols-lg-3">
                        <xsl:for-each select="Contenidos/contenido">
                            <xsl:for-each select="atributos/atributo">
                                <div class="feature col">
                                    <xsl:if test="./@nombre='NOMBRE'">
                                        <h2>
                                            <xsl:value-of select="."/>
                                        </h2>
                                    </xsl:if>

                                    <xsl:if test="./@nombre='DATOSCONTACTOS'">
                                        <xsl:for-each select="atributo">
                                            <xsl:if test="./@nombre='TELEFONO'">
                                                <p>
                                                    <xsl:value-of select="."/>
                                                </p>
                                            </xsl:if>
                                        </xsl:for-each>
                                    </xsl:if>
                                </div>
                            </xsl:for-each>
                        </xsl:for-each>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>