class Biblioteca{
    constructor(nombre, zonas){
        this.nombre = nombre;
        this.zonas = zonas;
    }

    libroConMasPaginas( numPaginas ){
        // Creamos el array donde guardaremos los resultados
        var resultadosLibros = []
    
       
        // Recorremos el array de biblioteca con cada una de las zonas
        for (let index = 0; index < this.zonas.length; index++) {
            // Asignamos cada zona a la variable elementZona
            const elementZona = this.zonas[index];
            // Recorremos el array de libros de cada una de las zonas
            for (let index = 0; index < elementZona['libros'].length; index++) {

                // Asignamos cada libro a la variable elementLibro
                const elementLibro = elementZona['libros'][index];

                // Comprobamos si el numero de paginas es mayor o igual al numero de páginas introducido como parametro
                if(elementLibro['paginas'] >= numPaginas){
                    // Añadimos al array de resultados el libro que cumple la condición
                    resultadosLibros.push(elementLibro)
                }
            }
        }
        return resultadosLibros;
    }

    loadZonas(zonas){
        for (let index = 0; index < zonas.length; index++) {
            const element = zonas[index];
            var librosTmp = [];
            for (let index = 0; index < element['libros'].length; index++) {
                const elementLibro = element['libros'][index];
                var libro = new Libro(
                    elementLibro['titulo'],
                    elementLibro['autor'],
                    elementLibro['paginas'],
                    elementLibro['editorial'],
                    );
                librosTmp.push(libro)
            }
            var zona = new Zona(
                element['planta'],
                element['responsable'],
                librosTmp)            
            this.zonas.push(zona);
        }        
    }

    loadData(data){
        this.nombre = data.nombre;
        this.loadZonas(data.zonas);
    }

    loadJson(url){
        var request = new XMLHttpRequest();
        request.open('GET',url);
        request.onload = function(){
            if (request.status >= 200 && request.status < 400){
                biblioteca.loadData( biblioteca.JSONtransformToObject(request.responseText));
            }
        }
        request.send()
    }
    
    transformJSON(){
        return JSON.stringify(Object.assign({}, this));  // convert array to string
    }

    JSONtransformToObject(JSONString){
        return JSON.parse(JSONString);  // convert string to json object
    }
}