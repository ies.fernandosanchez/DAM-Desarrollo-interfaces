const zona1 = {
    
    planta: "Planta Baja",
    
    responsable: "Lluís Biset",
    
    libros: [ 
        
        {
            
            titulo: "Les closques",
            
            autor: "Laia Viñas",
            
            paginas: 176,
            
            editorial: "Altra Editorial"
            
        },
        
        {
            
            titulo: "Napalm al cor",
            
            autor: "Pol Guasch",
            
            paginas: 240,
            
            editorial: "Anagrama"
            
        },
        
        {
            
            titulo: "El jardí de vidre",
            
            autor: "Tatiana Tibuleac",
            
            paginas: 328,
            
            editorial: "Les hores"
            
        },
        
    ]
    
}



const zona2 = {
    
    planta: "Planta 1a",
    
    responsable: "Andreu Parrello",
    
    libros: [
        
        {
            
            titulo: "El fil invisible",
            
            autor: "Miriam Tirado",
            
            paginas: 64,
            
            editorial: "B de Blok"
            
        },
        
        {
            
            titulo: "El dia que vaig marxar",
            
            autor: "Albert Om",
            
            paginas: 176,
            
            editorial: "Univers"
            
        },
        
        {
            
            titulo: "Paraules que tu entendràs",
            
            autor: "Xavier Bosch",
            
            paginas: 496,
            
            editorial: "Columna Edicions"
            
        },
        
    ]
    
}