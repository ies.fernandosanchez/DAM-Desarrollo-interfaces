var biblioteca = [];
biblioteca.push(zona1)
biblioteca.push(zona2)

/* Devolver aquellos libros con numero de paginas mayor que el parametro introducido */
function libroConMasPaginas( numPaginas ){
    // Creamos el array donde guardaremos los resultados
    var resultadosLibros = []

    // Recorremos el array de biblioteca con cada una de las zonas
    for (let index = 0; index < biblioteca.length; index++) {
        // Asignamos cada zona a la variable elementZona
        const elementZona = biblioteca[index];

        // Recorremos el array de libros de cada una de las zonas
        for (let index = 0; index < elementZona['libros'].length; index++) {

            // Asignamos cada libro a la variable elementLibro
            const elementLibro = elementZona['libros'][index];

            // Comprobamos si el numero de paginas es mayor o igual al numero de páginas introducido como parametro
            if(elementLibro['paginas'] >= numPaginas){
                // Añadimos al array de resultados el libro que cumple la condición
                resultadosLibros.push(elementLibro)
            }
        }
    }
    return resultadosLibros;
}

function getNumLibros(){
    var resultNumLibros = []
    for (let index = 0; index < biblioteca.length; index++) {
        resultNumLibros[biblioteca[index]['planta']] = biblioteca[index]['libros'].length;
    }
    return resultNumLibros;
}


function getResponsables(){
    var resultNumLibros = []
    for (let index = 0; index < biblioteca.length; index++) {
        resultNumLibros[biblioteca[index]['planta']] = biblioteca[index]['responsable'];
    }
    return resultNumLibros;
}
