//   Clase que representa 
//   una máquina de café
class MaquinaCafe{ 
    constructor(){
        this.nivelAzucar = 0;
        this.tipoCafe = [
            {
                'nombre':'expreso',
                'precio': 0.25
            }
            ]
        this.cafeSeleccionado = null;
        this.caja = 0;

            //   con leche','descafeinado','bombon'
    }

    añadirAzucar(){
        if(this.nivelAzucar < 10){
            this.nivelAzucar++; 
            this.pintaNivelAzucar();
        }       
    }
    
    quitarAzucar(){
        if(this.nivelAzucar > 0){
            this.nivelAzucar--; 
            this.pintaNivelAzucar();
        }
    }
    
    pintaNivelAzucar(){
        document.getElementById("nivelAzucar").style = 'width: '+this.nivelAzucar+'0%;';
    }

    seleccionaCafe( tipoCafe ){
        this.cafeSeleccionado = tipoCafe;
        this.habilitarBotonServirCafe()
    }

    habilitarBotonServirCafe(){
        document.getElementById("btnServir").disabled = false;
        document.getElementById("btnServir").classList.remove('btn-primary')
        document.getElementById("btnServir").classList.add('btn-info')
    }

    buscarCafe(){
        
        for (let index = 0; index < this.tipoCafe.length; index++) {
            const elementTipoCafe = this.tipoCafe[index];
            if(elementTipoCafe.nombre == this.cafeSeleccionado){
                return elementTipoCafe.precio;
            }
        }
        return false;    
    }

    servir(){
        
        this.nivelAzucar = 0;
        this.pintaNivelAzucar();
        var precio = this.buscarCafe()
        this.caja += precio;
        this.cafeSeleccionado = null;

    }
}