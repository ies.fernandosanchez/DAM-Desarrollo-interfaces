class Calculadora{

    constructor(){
        this.operador1 = null;
        this.operador2 = null;
        this.operacion = null;
    }

    reset(){
        this.operador1 = null;
        this.operador2 = null;
        this.operacion = null;
    }

    resetResultado(){
        document.getElementById("resultado").innerHTML = ''
    }

    calculaOperacion(){
        var resultado;
        if(this.operacion == '+'){
            resultado = this.operador1 + this.operador2;
        }else if(operacion == '-'){
            resultado = this.operador1 - this.operador2;
        }else{
            resultado = this.operador1 * this.operador2;
        }
        document.getElementById("resultado").innerHTML += '='+resultado
    }

    leerOperador(numero){
        // Si el operador 1 es distinto de null
        // entonces me estan pasando el operador 2
        if(this.operador1 != null && this.operacion != null){ 
            this.operador2 = numero;
            document.getElementById("resultado").innerHTML += this.operador2;
            this.calculaOperacion()
            this.reset();
        }else{
            this.operador1 = numero;
            this.resetResultado()
            document.getElementById("resultado").innerHTML = this.operador1;
        }
    }

    leerOperacion(operacionTmp){
        this.operacion = operacionTmp;
        document.getElementById("resultado").innerHTML += this.operacion;
    }
}